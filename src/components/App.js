import React, { useState, useEffect } from "react";
import "./App.css";

const App = () => {
  //use useState
  const [count, setCount] = useState(0);

  //use useEffect every state change
  useEffect(() => {
    document.title = "Clicked " + count + " times";
  });

  const increment = e => {
    if (e.target.id === "add") {
      setCount(count + 1);
    } else if (e.target.id === "substract" && count > 0) {
      setCount(count - 1);
    } else if (e.target.id === "reset") {
      setCount(count * 0);
    }
  };

  return (
    <div className="container">
      <h2 className="title">Contador : {count} </h2>
      <div className="buttons">
        <button id="add" onClick={increment}>
          {" "}
          +{" "}
        </button>
        <button id="substract" onClick={increment}>
          {" "}
          -{" "}
        </button>
        <button id="reset" onClick={increment}>
          {" "}
          Reset{" "}
        </button>
      </div>
    </div>
  );
};

export default App;
